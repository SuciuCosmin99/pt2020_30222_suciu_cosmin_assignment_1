/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polinom;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.EventListenerList;

public class Panel extends JPanel {

	private static final long serialVersionUID = 1L;
	private final EventListenerList listenerList = new EventListenerList();
	private JDialog d;

	public Panel() {
		Dimension size = getPreferredSize();
		size.width = 800;
		setPreferredSize(size);
		setBorder(BorderFactory.createTitledBorder("Polinom"));

		JLabel polinom1 = new JLabel("Polinom1:");
		final JTextField pol1txt = new JTextField(10);
		JLabel polinom2 = new JLabel("Polinom2:");
		final JTextField pol2txt = new JTextField(10);
		JLabel rez = new JLabel("Rezultat:");
		final JTextField textArea = new JTextField(10);

		JButton buton1 = new JButton("+");
		JButton buton2 = new JButton("-");
		JButton buton3 = new JButton("*");
		JButton buton4 = new JButton("/");
		JButton buton5 = new JButton("CLR");
		JButton buton6 = new JButton("(Polinom1)'");
		JButton buton7 = new JButton("(Polinom2)'");
		JButton buton8 = new JButton("∫ Polinom1");
		JButton buton9 = new JButton("∫ Polinom2");

		d = new Dialog();
		buton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom1 = new Polinom();
					Polinom polinom2 = new Polinom();
					Polinom rez = new Polinom();
					polinom1.parseString(pol1txt.getText());
					polinom2.parseString(pol2txt.getText());
					rez = Polinom.Adunare(polinom1, polinom2);
					String text = "Adunare:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		buton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom1 = new Polinom();
					Polinom polinom2 = new Polinom();
					Polinom rez = new Polinom();
					polinom1.parseString(pol1txt.getText());
					polinom2.parseString(pol2txt.getText());
					rez = Polinom.Scadere(polinom1, polinom2);
					String text = "Scadere:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		buton3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom1 = new Polinom();
					Polinom polinom2 = new Polinom();
					Polinom rez = new Polinom();
					polinom1.parseString(pol1txt.getText());
					polinom2.parseString(pol2txt.getText());
					rez = Polinom.Inmultire(polinom1, polinom2);
					String text = "Inmultire:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		buton4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});

		buton5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pol1txt.setText("");
				pol2txt.setText("");
				textArea.setText("");
			}
		});

		buton6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom1 = new Polinom();
					Polinom rez = new Polinom();
					polinom1.parseString(pol1txt.getText());
					rez = Polinom.Deriveaza(polinom1);
					String text = "Derivata1:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		buton7.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom2 = new Polinom();
					Polinom rez = new Polinom();
					polinom2.parseString(pol2txt.getText());
					rez = Polinom.Deriveaza(polinom2);
					String text = "Derivata2:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		buton8.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom1 = new Polinom();
					Polinom rez = new Polinom();
					polinom1.parseString(pol1txt.getText());
					rez = Polinom.Integreaza(polinom1);
					String text = "Integrala1:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		buton9.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Polinom polinom2 = new Polinom();
					Polinom rez = new Polinom();
					polinom2.parseString(pol2txt.getText());
					rez = Polinom.Integreaza(polinom2);
					String text = "Integrala2:" + rez.toString() + "\n";
					textArea.setText(text);
					fireDetEvent(new Event(this, text));
				} catch (Exception exp) {
					d.setVisible(true);
				}
			}
		});

		setLayout(new GridBagLayout());

		GridBagConstraints gc = new GridBagConstraints();

		gc.anchor = GridBagConstraints.LINE_END;
		gc.weightx = 0.5;
		gc.weighty = 0.5;

		gc.gridx = 0;
		gc.gridy = 0;
		add(polinom1, gc);

		gc.gridx = 0;
		gc.gridy = 1;
		add(polinom2, gc);

		gc.gridx = 2;
		gc.gridy = 0;
		add(buton5, gc);

		gc.anchor = GridBagConstraints.LINE_START;
		gc.gridx = 1;
		gc.gridy = 0;
		add(pol1txt, gc);

		gc.gridx = 1;
		gc.gridy = 1;
		add(pol2txt, gc);

		gc.anchor = GridBagConstraints.FIRST_LINE_START;
		gc.gridx = 0;
		gc.gridy = 2;
		add(buton1, gc);

		gc.gridx = 1;
		gc.gridy = 2;
		add(buton2, gc);

		gc.gridx = 2;
		gc.gridy = 2;
		add(buton3, gc);

		gc.gridx = 3;
		gc.gridy = 2;
		add(buton4, gc);

		gc.gridx = 0;
		gc.gridy = 3;
		add(buton6, gc);

		gc.gridx = 1;
		gc.gridy = 3;
		add(buton7, gc);

		gc.gridx = 2;
		gc.gridy = 3;
		add(buton8, gc);

		gc.gridx = 3;
		gc.gridy = 3;
		add(buton9, gc);

		gc.anchor = GridBagConstraints.LINE_END;
		gc.gridx = 0;
		gc.gridy = 4;
		add(rez, gc);

		gc.anchor = GridBagConstraints.LINE_START;
		gc.ipadx = 200;
		gc.ipady = 200;
		gc.gridx = 1;
		gc.gridy = 4;
		add(textArea, gc);
	}

	public void fireDetEvent(Event event) {
		int i;
		Object[] listeners = listenerList.getListenerList();
		for (i = 0; i < listeners.length; i += 2)
			if (listeners[i] == ListenerImpl.class) {
				((ListenerImpl) listeners[i + 1]).detailEventOccurred(event);
			}
	}

	public void addDetailListener(ListenerImpl listener) {
		listenerList.add(ListenerImpl.class, listener);
	}

	public void removeDetailListener(ListenerImpl listener) {
		listenerList.remove(ListenerImpl.class, listener);
	}
}
