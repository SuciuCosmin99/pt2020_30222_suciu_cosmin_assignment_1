package polinom;

import java.util.*;

//Comparable este clasa predefinita
public class Monom implements Comparable {
	public double coef;
	public double putere;

	public Monom(double coef, double putere) {
		this.coef = coef;
		this.putere = putere;
	}

	public Monom() {
		this.coef = 0;
		this.putere = 0;
	}

	public double getCoef() {
		return this.coef;
	}

	public double getPutere() {
		return this.putere;
	}

	public void setCoef(double d) {
		this.coef = d;
	}

	public void setPutere(double d) {
		this.putere = d;
	}
	
	public static Monom copy(Monom m) {
		return new Monom(m.getCoef(), m.getPutere());
	}
	
	public static Monom adunareMonom(Monom m1, Monom m2) {
		Monom rezultat;
		if (m1.getPutere() != m2.getPutere())
			return null;
		rezultat = Monom.copy(m1);
		rezultat.setCoef(rezultat.getCoef() + m2.getCoef());
		return rezultat;
	}

	public static Monom inmultireMonom(Monom m1, Monom m2) {
		Monom rezultat = Monom.copy(m1);
		rezultat.setCoef(rezultat.getCoef() * m2.getCoef());
		rezultat.setPutere(rezultat.getPutere() + m2.getPutere());
		return rezultat;
	}

	public static Monom deriveazaMonom(Monom m1) {
		Monom rezultat = Monom.copy(m1);
		rezultat.setCoef(rezultat.getCoef() * rezultat.getPutere());
		rezultat.setPutere(rezultat.getPutere() - 1);
		return rezultat;
	}

	public static Monom integreazaMonom(Monom m1) {
		Monom rezultat = Monom.copy(m1);
		rezultat.setCoef(rezultat.getCoef() / (rezultat.getPutere() + 1));
		rezultat.setPutere(rezultat.getPutere() + 1);
		return rezultat;
	}

	@Override
	public boolean equals(Object arg0) {
		Monom m = (Monom) arg0;
		if ((this.getCoef() == m.getCoef()) && (this.getPutere() == m.getPutere()))
			return true;
		else
			return false;
	}

	public static Comparator<Monom> compGrad = new Comparator<Monom>() {
		@Override
		public int compare(Monom m1, Monom m2) {
			return (int) -(m1.getPutere() - m2.getPutere());
		}
	};

	@Override
	public int compareTo(Object o) {
		Monom m = (Monom) o;
		if (this.getPutere() != m.getPutere())
			return (int) (this.getPutere() - m.getPutere());
		else
			return (int) (this.getCoef() - m.getCoef());
	}

}