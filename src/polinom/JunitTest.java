package polinom;

import org.junit.Assert;
import org.junit.Test;

public class JunitTest {

	@Test
	public void test() {
		Monom m1 = new Monom(3, 3);
		Monom m2 = new Monom(3, 2);
		Monom m3 = new Monom(2, 2);
		Monom m4 = new Monom(2, 4);
		Polinom radd = new Polinom();
		Polinom rsub = new Polinom();
		Polinom rder = new Polinom();
		Polinom rint = new Polinom();
		Polinom rmul = new Polinom();
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.addMonom(m1);
		p1.addMonom(m2);
		p2.addMonom(m3);
		p2.addMonom(m4);
		rsub = Polinom.Scadere(p1, p2);
		rsub.Afisare();
		Assert.assertEquals("3x^3-2x^4+1x^2", rsub.toString());
		System.out.println();
		radd = Polinom.Adunare(p1, p2);
		radd.Afisare();
		Assert.assertEquals("3x^3+2x^4+5x^2", radd.toString());
		System.out.println();
		rder = Polinom.Deriveaza(p1);
		rder.Afisare();
		Assert.assertEquals("9x^2+6x^1", rder.toString());
		System.out.println();
		rint = Polinom.Integreaza(p1);
		rint.AfisareIntegrare();
		Assert.assertEquals("0.75x^4+1x^3", rint.toString());
		System.out.println();
		rmul = Polinom.Inmultire(p1, p2);
		rmul.Afisare();
		Assert.assertEquals("6x^7+6x^5+6x^6+6x^4", rmul.toString());
		System.out.println();
	}

	@Test
	public void testParse() {
		String string = "1x^3+a";
		boolean notValid = false;

		try {

			Polinom p1 = new Polinom();
			p1 = p1.parseString(string);
		} catch (Exception e) {
			notValid = true;
		}
		Assert.assertEquals(true, notValid);
		
		
		String sstring = "1x^3";
		boolean Valid = true;
		try {

			Polinom p1 = new Polinom();
			p1 = p1.parseString(sstring);
		} catch (Exception e) {
			Valid = false;
		}
		Assert.assertEquals(true, Valid);
	}

}
