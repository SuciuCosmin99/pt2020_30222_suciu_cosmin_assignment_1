package polinom;

import java.util.EventListener;

public interface ListenerImpl extends EventListener {
	public void detailEventOccurred(Event event);

}
