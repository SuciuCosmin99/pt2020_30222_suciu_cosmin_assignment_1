package polinom;

import javax.swing.*;
import javax.swing.SwingUtilities;

public class GUI {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new Frame("POLYNOMIAL CALCULATOR");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1200, 800);
				frame.setVisible(true);
			}
		});

	}
}
