package polinom;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom {
	ArrayList<Monom> M;
	double grad;

	public Polinom() {
		M = new ArrayList<>();
		grad = 0;
	}

	public double getGrad() {
		return grad;
	}

	public int eGrad(double d) {
		for (int i = 0; i < M.size(); i++)
			if (d == M.get(i).getPutere())
				return i;
		return -1;
	}// avand nevoie de index nu am folosit for each
	
	public void addMonom(Monom m) {
		int i = eGrad(m.getPutere());
		if (i == -1) {
			M.add(m);
			if (m.getPutere() > grad)
				grad = m.getPutere();
		} else {
			Monom mon = M.get(i);
			M.remove(i);
			mon = Monom.adunareMonom(m, mon);
			if (mon.getCoef() == 0)
				calculeazaGrad();
			else
				M.add(mon);

		}
	}
	
	private void calculeazaGrad() {
		double maxim = 0;
		for (Monom monom : M)
			if (monom.getPutere() > maxim)
				maxim = monom.getPutere();
		grad = maxim;
	}

	public ArrayList<Monom> repPolinom() {
		ArrayList<Monom> p1 = new ArrayList<>();
		for (Monom monom : M)
			p1.add(Monom.copy(monom));
		Collections.sort(p1, Monom.compGrad);
		return p1;
	}

	public Polinom parseString(String s) {
		/*
		Polinom p = new Polinom();
		Pattern pattern = Pattern.compile("(-?\\b\\d+)x\\^(-?\\d+\\b)");
		Matcher matcher = pattern.matcher(s);
		while (matcher.find()) {
			int coeficient = Integer.parseInt(matcher.group(1));
			int putere = Integer.parseInt(matcher.group(2));
			Monom m = new Monom(coeficient, putere);
			M.add(m);
		}
		return p;
		*/
		
		 
		Polinom p = new Polinom(); 
	    for (String x : s.split("\\+", 0)) { 
	  		String[] y = x.split("\\^", 0); 
	  		String[] z = y[0].split("x", 0); 
	  		int coeficient = Integer.parseInt(z[0]); 
		  	int putere = Integer.parseInt(y[1]); 
	 		Monom m = new Monom(coeficient, putere); 
	 		M.add(m); 
	 	} 
	    return p;
		//parsarea cu regex imi anuleaza testul junit si afisarea unui dialog, deoarece el ia doar ce e bun din stringul dat, de exemplu din 2x^2+a -> 2x^2

	}

	@Override
	public String toString() {
		String string = "";
		if (this.M.size() != 0) {
			for (Monom m : M) {
				if (m.getCoef() != 0)
					string = string + m.getCoef() + "x^" + m.getPutere() + "+";
			}
		}
		if (string != "") {

			string = string.replace("^1+", "+");
			string = string.replace("x^0", "");
			string = string.replace(".0", "");
			string = string.replace("+-", "-");
			string = string.substring(0, string.length() - 1);
		}
		return string;
	}

	public void Afisare() {
		ArrayList<Monom> pol1 = repPolinom();
		for (Monom monom : pol1)
			System.out.print(
					(((int) monom.getCoef() > 0) ? "+" : "") + (int) monom.getCoef() + "x^" + (int) monom.getPutere());
	}

	public void AfisareIntegrare() {
		ArrayList<Monom> pol1 = repPolinom();
		for (Monom monom : pol1)
			System.out.print(((monom.getCoef() > 0) ? "+" : "") + monom.getCoef() + "x^" + (int) monom.getPutere());
	}

	public static Polinom Adunare(Polinom P1, Polinom P2) {
		ArrayList<Monom> p1 = P1.repPolinom();
		ArrayList<Monom> p2 = P2.repPolinom();
		Polinom rezultat = new Polinom();
		for (Monom monom : p1)
			rezultat.addMonom(monom);
		for (Monom monom2 : p2)
			rezultat.addMonom(monom2);
		return rezultat;

	}

	public static Polinom Scadere(Polinom P1, Polinom P2) {
		ArrayList<Monom> p1 = P1.repPolinom();
		ArrayList<Monom> p2 = P2.repPolinom();
		Polinom rezultat = new Polinom();
		Monom m = new Monom();
		for (Monom monom : p1)
			rezultat.addMonom(monom);
		for (Monom monom2 : p2) {
			m = Monom.copy(monom2);
			m.setCoef(m.getCoef() * (-1));
			rezultat.addMonom(m);
		}
		return rezultat;
	}

	public static Polinom Inmultire(Polinom P1, Polinom P2) {
		ArrayList<Monom> p1 = P1.repPolinom();
		ArrayList<Monom> p2 = P2.repPolinom();
		Polinom rezultat = new Polinom();
		for (Monom monom : p1) {
			for (Monom monom2 : p2)
				rezultat.addMonom(Monom.inmultireMonom(monom, monom2));
		}
		return rezultat;
	}

	public Polinom PCopy() {
		ArrayList<Monom> L = this.repPolinom();
		Polinom p1 = new Polinom();
		for (Monom monom : L)
			p1.addMonom(Monom.copy(monom));
		return p1;
	}

	public Monom getMonomMaxim() {
		ArrayList<Monom> M = this.repPolinom();
		Monom max = new Monom(0, 0);
		for (Monom monom : M)
			if (monom.compareTo(max) > 0)
				max = Monom.copy(monom);
		return max;
	}

	public static Polinom Deriveaza(Polinom P1) {
		ArrayList<Monom> p1 = P1.repPolinom();
		Polinom rezultat = new Polinom();
		for (Monom monom : p1)
			rezultat.addMonom(Monom.deriveazaMonom(monom));
		return rezultat;
	}

	public static Polinom Integreaza(Polinom P1) {
		ArrayList<Monom> p1 = P1.repPolinom();
		Polinom rezultat = new Polinom();
		for (Monom monom : p1)
			rezultat.addMonom(Monom.integreazaMonom(monom));
		return rezultat;
	}

}
