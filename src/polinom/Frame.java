package polinom;

import java.awt.*;
import javax.swing.*;

public class Frame extends JFrame {

	private static final long serialVersionUID = 1L;
	private final Panel detailsPanel;

	public Frame(String title) {
		super(title);
		// Set layout
		setLayout(new BorderLayout());

		// create component
		final JTextArea textArea = new JTextArea();

		detailsPanel = new Panel();

		detailsPanel.addDetailListener(new ListenerImpl() {
			@Override
			public void detailEventOccurred(Event event) {
				String text = event.getText();
				textArea.append(text);
			}
		});

		// container
		Container c = getContentPane();

		c.add(textArea, BorderLayout.CENTER);
		c.add(detailsPanel, BorderLayout.WEST);

	}

}
