package polinom;

import java.util.EventObject;

public class Event extends EventObject {

	private static final long serialVersionUID = 1L;
	private final String text;

	public Event(Object source, String text) {
		super(source);
		this.text = text;
	}

	public String getText() {
		return text;
	}
}
